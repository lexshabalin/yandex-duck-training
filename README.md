- СТРАНИЦА ТРЕНИРОВОК - https://yandex.ru/yaintern/training/devops-training
- РЕЗУЛЬТАТ - https://yastatic.net/s3/anytask/shmya/%D0%B8%D1%82%D0%BE%D0%B3%D0%BE%D0%B2%D1%8B%D0%B9_%D1%80%D0%B5%D0%B9%D1%82%D0%B8%D0%BD%D0%B3.xlsx

![Hello](https://media.tenor.com/2ZNExnM_7F0AAAAC/hello-there.gif)

## План проекта:
- Пакуем все нужности\полезности packer'ом
- Разворачиваем структуру терраформом.
- Проводим тонкую донастройку с помощью ansible

## Инструменты использованные в проекте:

- ![Packer](https://www.packer.io/)
- ![Vault](https://www.vaultproject.io/)
- ![Terraform](https://www.terraform.io/)
- ![Yandex cloud](https://cloud.yandex.ru/)
- ![Nginx](https://www.nginx.com/)

## Packer

Настоящая боль - **bingo prepare_db**. Очень не понравилось ждать подготовку. Мало того, такая подготовка каждый раз - бесполезная трата времени и финансов (поднимаемся же в облаке, за денежку). Заполним базу 1 раз и зафиксируем все пакером, чтобы не мучаться.
У меня уже был опыт работы с **packer** и были кое-какие наработки, потому я просто переиспользовал пару инструкций из другого проекта. К тому же там уже были прописаны инсталяции **ansible** и **vault**. 

С инструкциями по сборке можно ознакомиться в ![./packer](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/packer)
## Terraform

Структура состоит из следующих узлов:
- nginx proxy    | www.bingo.oorange.space
- vm1 (app1) | www.app1.oorange.space
- vm2 (app2) | www.app2.oorange.space
- database vm    | www.db.oorange.space

Точка входа - балансировщик **nginx**, который распределяет трафик между **app1** и **app2**. Они же в свою очередь стучатся к **db_vm**.

Все секреты терраформа хранятся в **vault**, который развернут у меня дома в **docker** на ![orangePI](http://www.orangepi.org/).

![Мой vault](https://gitlab.com/lexshabalin/yandex-duck-training/raw/master/pic/vault.jpg)

В ![main.tf](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/terraform/main.tf) расписана вся структура, достаточно доходчиво и с комментариями, так что подробно обрисовывать не стану. Укажу на некоторые тонкости:

- Разворачиваются все инстансы из образов, заботливо упакованных **packer'ом**. **Id** образов хранятся в **vault**, как видно на скриншоте.
- Т.к. инстансы каждый раз при создании получают случайный ip-адрес, а я использую **ansible** для донастройки, имеет смысл запускать плейбуки локально. Продолжаем экономить время.
- **Nginx** должен разорачиваться в последнюю очередь, чтобы сразу подхватить **upstream** без ошибок, потому он начинает создаваться только после второго **load_balancer**.

## Ansible

**Bingo playbook**

Благодаря **strace** я нашел путь к конфигу *bingo* (/opt/bingo/config.yaml) и файл для записи логов (/opt/bongo/logs/9fa47c93ec/main.log). А **ss -tlpn** показал мне порт, на котором приложение принимает коннекты.

- Подкидываем свежий конфиг для бинго, вместо того, что был зашит packer'ом изначально, при сборке образа
- Добавляем ![bingo.service](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/bingo.service)
- Прокидываем права на файл пользователю в **/opt/bongo/logs/9fa47c93ec/main.log**
- Добавляем ![healthcheck.service](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/healthcheck/healthcheck.service) и ![healthcheck.timer](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/healthcheck/healthcheck.timer)
- Копируем **netplan config** и применяем его
- Daemon reload
- Enable+start все добавленные сервисы и таймеры


*Лирическое отступление* 
- Что делают ![healthcheck.service](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/healthcheck/healthcheck.service) и ![healthcheck.timer](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/healthcheck/healthcheck.timer)?


Я так и не смог ускорить запуск приложения, сколько бы я не вчитывался в его strace. Киляя дочерние процессы, которые очевидно замедляют запуск, я прерывал запуск всего сервера, потому решил грызть сосиску с другой стороны. Каждую секунду на сервере с **bingo** происходит **"curl -s http://localhost:8380/ping"** и если ответ отличается от *"pong"*, то **bingo.service** перезагружается. Приложение стартует 30 секунд +примерно 1-2 секунды на отработку ![скрипта](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/app/healthcheck/healthcheck.sh). ТЗ соблюдено. **PROFIT** 


- Что за **netplan**?


Во время работы над прошлым проектом на **yandex cloud** я заметил, что **dns** может долго подсасываться и замедлять деплой моих сервисов. В моем ![01-netconfig.yaml](https://gitlab.com/lexshabalin/yandex-duck-training/-/blob/master/misc/01-netcfg.yaml) просто прописаны адреса **dns** серверов гугла и яндекса для ускорения процесса.

**Database playbook**

- Открываем порт pgbouncer (6432)
- Подкидываем преднастроенные **pg_hba.conf** и **postgresql.conf**
- Индексируем базу данных 
- Останавливаем **postgresql**
- Устанавливаем и настраиваем **pgbouncer**
- Стартуем **pgbouncer** и **postgresql**
- Перезагружаем сервер

*Лирическое отступление №2*
- Зачем перезагрузка сервера?

Какие бы я действия не делал с ufw для открытия порта 6432, после применения правила - ничего не помагало. Снова экономим время и решаем проблему радикально.


**Nginx playbook**

- Копируем и применяем **netplan config**
- Подкидываем конфиг **nginx**
- БЛОК С ПОДГОТОВКОЙ К HTTPS, КОТОРЫЙ Я НЕ УСПЕЛ РЕАЛИЗОВАТЬ :C
- Перезагружаем **nginx**

# ИТОГО


Структура, поднимающаяся по команде **terraform apply --auto-approve** которая запускает nginx load balancer, баз уданных и 2 узла **bingo app**, которые отслеживаются самописным мониторингом из ~~говна и палок~~ **bash** и **systemd.timer**.

С нетерпением жду обратной связи!
- lexshabalin@gmail.com 
- tg:@Alex_Shabalin

![Bye!](https://media.tenor.com/MNYCCz5JSm8AAAAC/toy-story-buzz-lightyear.gif)
