source "yandex" "yc-toolbox" {
  folder_id           = "b1gqhaovf5mlgu9qrsii"
  source_image_family = "ubuntu-2004-lts"
  ssh_username        = "ubuntu"
  use_ipv4_nat        = "true"
  image_description   = "Yandex Cloud Ubuntu Toolbox image"
  image_family        = "my-images"
  image_name          = "bingo"
  subnet_id           = "b0cg9u6hfm5upqe6kgqc"
  disk_type           = "network-hdd"
  zone                = "ru-central1-c"
}

build {
  sources = ["source.yandex.yc-toolbox"]

  provisioner "file" {
    source = "/home/rh/.bashrc"
    destination = "/tmp/.bashrc"
} 

  provisioner "shell" {
    inline = [

      # Create dir for file provisioner
      "sudo mkdir /opt/cfg",
      "sudo cp /tmp/.bashrc /etc/bash.bashrc",

      # Global Ubuntu things
      "sudo apt-get update",
      "echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections",

      # Ansible install
      "sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py",
      "python3 get-pip.py --user",
      "python3 -m pip install --user ansible",
      "sudo apt install ansible ca-certificates curl git gnupg lsb-release unzip mc preload -y",

      # git clone
      "sudo mkdir /opt/git",
      "cd /opt/git",
      "sudo git init .",
      "sudo git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",

      # Training stuff
      "sudo mv /opt/git/yandex-duck-training/app/bingo /usr/local/bin/bingo",
      "sudo mkdir /opt/bingo",
      "sudo mkdir /opt/bongo",
      "sudo mkdir /opt/bongo/logs",
      "sudo mkdir /opt/bongo/logs/9fa47c93ec",
      "sudo touch /opt/bongo/logs/9fa47c93ec/main.log",
      "sudo cp /opt/git/yandex-duck-training/app/config.yaml /opt/bingo/config-default.yaml"

    ]
  }
}

