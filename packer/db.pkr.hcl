source "yandex" "yc-toolbox" {
  folder_id           = "b1gqhaovf5mlgu9qrsii"
  source_image_family = "ubuntu-2004-lts"
  ssh_username        = "ubuntu"
  use_ipv4_nat        = "true"
  image_description   = "Yandex Cloud Ubuntu Toolbox image"
  image_family        = "my-images"
  image_name          = "bingo-db-postgres"
  subnet_id           = "b0cg9u6hfm5upqe6kgqc"
  disk_type           = "network-hdd"
  zone                = "ru-central1-c"
}

build {
  sources = ["source.yandex.yc-toolbox"]

  provisioner "file" {
    source = "/home/rh/.bashrc"
    destination = "/tmp/.bashrc"
} 

  provisioner "file" {
    source = "/home/rh/yandex_training/final/db/dbinstall.sh"
    destination = "/tmp/dbinstall.sh"
}

  provisioner "shell" {
    inline = [

      # Create dir for file provisioner
      "sudo mkdir /opt/cfg",
      "sudo cp /tmp/.bashrc /etc/bash.bashrc",
      "sudo cp /tmp/dbinstall.sh /etc/dbinstall.sh",

      # Global Ubuntu things
      "cd /etc/",
      "./dbinstall.sh",
      "sudo apt-get update",
      "echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections",
      "sudo apt-get update",
      "sudo apt-get install -y postgresql postgresql-contrib",

      # Ansible install
      "sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py",
      "python3 get-pip.py --user",
      "python3 -m pip install --user ansible",
      "sudo apt install ansible ca-certificates curl git gnupg lsb-release unzip mc -y",

      # git clone
      "sudo mkdir /opt/git",
      "cd /opt/git",
      "sudo git init .",
      "sudo git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",

      # Training stuff
      "sudo mv /opt/git/yandex-duck-training/app/bingo /usr/local/bin/bingo",
      "sudo mkdir /opt/bingo",
      "sudo cp /opt/git/yandex-duck-training/app/config-default.yaml  /opt/bingo/config.yaml",
      "cd /opt/git/yandex-duck-training/misc",
      "./alter_user_default.sh",
      "bingo prepare_db"

    ]
  }
}

