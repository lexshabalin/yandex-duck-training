terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "vault" {
  address = "http://192.168.1.17:8200"
#  address = "http://109.111.174.147:8200"
  skip_tls_verify = true
}

#########################################################################################################################################################################
######################################################################## HASHICORP VAULT SECRETS ########################################################################
#########################################################################################################################################################################

data "vault_generic_secret" "yc_secrets" {
  path = "yandex-training/duck_secrets"
}

provider "yandex" {
  token     = data.vault_generic_secret.yc_secrets.data["YC_TOKEN"]
  cloud_id  = data.vault_generic_secret.yc_secrets.data["YC_CLOUD_ID"]
  folder_id = data.vault_generic_secret.yc_secrets.data["YC_FOLDER_ID"]
  zone      = "ru-central1-c"
}

data "yandex_compute_image" "db" {
  image_id = data.vault_generic_secret.yc_secrets.data["DB_IMAGE_ID"]
}

data "yandex_compute_image" "bingo" {
  image_id = data.vault_generic_secret.yc_secrets.data["BINGO_IMAGE_ID"]
}

data "yandex_compute_image" "nginx" {
  image_id = data.vault_generic_secret.yc_secrets.data["NGINX_IMAGE_ID"]
}


#########################################################################################################################################################################
########################################################################### DATABASE HOST ###############################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "db" {
  name        = "db"
  platform_id = "standard-v1"
  zone        = "ru-central1-c"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.db.id
      size     = 30
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname bingodb",
      "cd /home/rh",
      "git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",
      "ansible-playbook -b -c local -i localhost, yandex-duck-training/ansible/db.yml",
      "sudo cp /opt/git/yandex-duck-training/misc/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
}

#########################################################################################################################################################################
########################################################################### NGINX BALANCER ##############################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "nginx" {
  name        = "nginx"
  platform_id = "standard-v1"
  zone        = "ru-central1-c"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.nginx.id
      size     = 30
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname bingo-nginx",
      "cd /home/rh",
      "git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",
      "ansible-playbook -b -c local -i localhost, yandex-duck-training/ansible/nginx.yml",
      "sudo cp /home/rh/yandex-duck-training/misc/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
  depends_on = [yandex_compute_instance.app2]
}


#########################################################################################################################################################################
########################################################################### app1 ##############################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "app1" {
  name        = "app1"
  platform_id = "standard-v1"
  zone        = "ru-central1-c"

  resources {
   cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.bingo.id
      size     = 30
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname app1",
      "cd /home/rh",
      "git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",
      "ansible-playbook -b -c local -i localhost, yandex-duck-training/ansible/bingo.yml",
      "sudo cp /home/rh/yandex-duck-training/misc/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
}


#########################################################################################################################################################################
########################################################################### app2 ##############################################################################
#########################################################################################################################################################################

resource "yandex_compute_instance" "app2" {
  name        = "app2"
  platform_id = "standard-v1"
  zone        = "ru-central1-c"

  resources {
   cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.bingo.id
      size     = 30
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-sn-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("./metadata.txt")}"
  }

  provisioner "file" {
    source      = "/home/rh/.bashrc"
    destination = "/home/rh/.bashrc"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "/home/rh/consulsrv.hcl-template"
    destination = "/home/rh/consulsrv.hcl-template"

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  ##remote+local exec
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname app2",
      "cd /home/rh",
      "git clone https://gitlab.com/lexshabalin/yandex-duck-training.git",
      "ansible-playbook -b -c local -i localhost, yandex-duck-training/ansible/bingo.yml",
      "sudo cp /home/rh/yandex-duck-training/misc/01-netcfg.yaml /etc/netplan/01-netcfg.yaml",
      "sudo netplan apply"
    ]

    connection {
      type        = "ssh"
      user        = "rh"
      private_key = file("~/.ssh/id_ed25519")
      host        = self.network_interface[0].nat_ip_address
    }
  }
}



#########################################################################################################################################################################                                                  
################################################################################# NETWORK ###############################################################################
#########################################################################################################################################################################

resource "yandex_vpc_network" "my-nw-1" {
  name = "my-nw-1"
}

resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "desc"

  labels = {
    label1 = "label-1-value"
  }

  zone   = "oorange.space."
  public = true
}

#########################################################################################################################################################################                                                  
################################################################################### DNS #################################################################################
#########################################################################################################################################################################

#DNS-overall
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "srv.oorange.space."
  type    = "A"
  ttl     = 200
  data    = ["10.1.0.1"]
}

#DNS for database    
resource "yandex_dns_recordset" "db" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.db"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.db.network_interface.0.nat_ip_address]
}

#DNS for bingoapp1
resource "yandex_dns_recordset" "app1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.app1"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.app1.network_interface.0.nat_ip_address]
}

#DNS for bingoapp2
resource "yandex_dns_recordset" "app2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.app2"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.app2.network_interface.0.nat_ip_address]
}

#DNS for nginx
resource "yandex_dns_recordset" "nginx" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.bingo"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.nginx.network_interface.0.nat_ip_address]
}

############################################ SUBNET
resource "yandex_vpc_subnet" "my-sn-1" {
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.my-nw-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

#########################################################################################################################################################################                                                  
################################################################################# OUTPUT ################################################################################
#########################################################################################################################################################################
